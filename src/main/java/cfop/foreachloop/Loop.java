package cfop.foreachloop;

public class Loop {

	public static void main(String[] args) {

			String[] tabs= {"One","Two","Three"};
			simpleLoop(tabs);
			System.out.println("------------------------------");
			loopWithForEach(tabs);
			
	}

	private static void loopWithForEach(String[] tabs) {
		for (String item : tabs) {
			System.out.println(item);
			
		}
	}

	private static void simpleLoop(String[] tabs) {
		for (int i = 0; i < tabs.length; i++) {
			System.out.println(tabs[i]);
		}
	}
	

}
