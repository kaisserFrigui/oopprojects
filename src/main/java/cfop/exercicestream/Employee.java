package cfop.exercicestream;

public class Employee {
	
	private String name;
	private int age;
	private double  salary;
	private String position;
	public Employee(String name, int age, double salary, String position) {
		super();
		this.name = name;
		this.age = age;
		this.salary = salary;
		this.position = position;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + ", salary=" + salary + ", position=" + position + "]";
	}
	
	
	
	

}
