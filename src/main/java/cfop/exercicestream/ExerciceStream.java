package cfop.exercicestream;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collector;
import java.util.stream.Stream;

public class ExerciceStream {

	public static void main(String[] args) {

		List<Employee> employees = new ArrayList<Employee>();
		employees.add(new Employee("Kaisser", 30, 100, "Developpeur"));
		employees.add(new Employee("Kaisser", 32, 150, "CEO"));
		employees.add(new Employee("Firas", 29, 200, "QA"));

		searchEmployeeByNameAndByPositionAndByAge(employees, new EmployeeSearch(30, "Developpeur", "Kaisser"))
				.stream().forEach((emp) -> System.out.println(emp.toString()));

	}

	private static List<Employee> searchEmployeeByNameAndByPositionAndByAge(List<Employee> employees,
			EmployeeSearch empSearch) {
		Predicate<Employee> search = (employee) -> employee.getName().equals(empSearch.getName())
				&& employee.getPosition().equals(empSearch.getPosition()) && employee.getAge() == empSearch.getAge();
		return employees.stream().filter(search).toList();

	}

}
