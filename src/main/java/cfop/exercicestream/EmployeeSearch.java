package cfop.exercicestream;

public class EmployeeSearch {
	
	private int age;
	private String position;
	private String name;

	public EmployeeSearch(int age, String position, String name) {
		super();
		this.age = age;
		this.position = position;
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	

}
