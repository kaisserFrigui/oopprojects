package cfop.specException;

public class UnuthorizedWithdrawException extends Exception {

	private String code;

	public UnuthorizedWithdrawException(String code,String message ) {
		super(message);
		this.code = code;
	}

	public String getCode() {
		return code;
	}
	
	

}
