package cfop.annotation;

@Departement(city = "MANOUBA", name = "CEO")
public class Manager {

	private long id;
	private String name;
	private String position;
	private int age;

	public Manager(long id, String name, String position, int age) {
		super();
		this.id = id;
		this.name = name;
		this.position = position;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setId(long id) {
		this.id = id;
	}

}
