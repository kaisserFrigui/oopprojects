package cfop.annotation;

@Departement
public class Employee {

	private long id;
	private String name;
	private String position;
	private int age;

	public Employee(long id, String name, String position, int age) {
		super();
		this.id = id;
		this.name = name;
		this.position = position;
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setId(long id) {
		this.id = id;
	}

}
