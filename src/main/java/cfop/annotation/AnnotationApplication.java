package cfop.annotation;

public class AnnotationApplication {

	public static void main(String[] args) {

		Employee employee = new Employee(1, "Kaisser", "MANOUBA", 30);
		System.out.println(employee.getClass().getAnnotation(Departement.class));
		Manager manager = new Manager(2, "Firas", "TUNIS", 29);
		System.out.println(manager.getClass().getAnnotation(Departement.class));

	}

}
