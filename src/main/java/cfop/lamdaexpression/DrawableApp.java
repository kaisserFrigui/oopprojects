package cfop.lamdaexpression;

public class DrawableApp {


	public static void main(String[] args) {
		Drawable drawableCircle=()-> System.out.println("Draw Circle");
		Drawable drawableRectangle=()-> System.out.println("Draw Rectangle");
		drawableCircle.draw();
		drawableRectangle.draw();
		

	}

}
