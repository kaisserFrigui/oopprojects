package cfop.atomic;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class ThreadPoolExecutorServiceAtomic {

	private AtomicInteger taskExecutedCount = new AtomicInteger(0);

	/*
	 * private void incrementAndReport() {
	 * System.out.println(Thread.currentThread().getName()+ " :"
	 * +taskExecutedCount.incrementAndGet() + " task (s)"); }
	 */

	private  void incrementAndReport() {
		System.out.println(Thread.currentThread().getName() + " : with state "+Thread.currentThread().getState()
				+" :  " + taskExecutedCount.incrementAndGet() + " task (s)");
	}

	public static void main(String[] args) {
		ExecutorService service = null;
		try {
			service = Executors.newFixedThreadPool(20); // Runtime.getRuntime().availableProcessors()
			ThreadPoolExecutorServiceAtomic incrementCountTaskService = new ThreadPoolExecutorServiceAtomic();

			for (int i = 0; i < 10; i++)
			{
				service.submit(() ->
						
						{
							incrementCountTaskService.incrementAndReport();
							
						});
				
					
				
			}
			
		} finally {
			if (service != null)
				service.shutdown();
		}
	}

}
