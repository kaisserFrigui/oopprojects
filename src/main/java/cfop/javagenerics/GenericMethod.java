package cfop.javagenerics;

import cfop.annotation.Departement;
import cfop.annotation.Employee;

public class GenericMethod {

	public static void main(String[] args) {

		genericDisplay(1);
		genericDisplay(1.50);
		genericDisplay("hhhhh");
		genericDisplay(new Employee(1, "aaa", "aaa", 0).getClass().getAnnotation(Departement.class));
	}
	public static <T> void genericDisplay(T type ) {
		System.out.println(type.toString());
	}
}
