package cfop.javagenerics;

import cfop.annotation.Departement;
import cfop.annotation.Employee;

public class DisplayClassGenericApp {

	public static void main(String[] args) {
		GenericClassDisplay<String> displayString= new GenericClassDisplay<String>("Test");
		GenericClassDisplay<Integer> displayinteger= new GenericClassDisplay<Integer>(1);
		GenericClassDisplay<Boolean> displayboolean= new GenericClassDisplay<Boolean>(true);
		GenericClassDisplay<Departement> displayEmployyeDepartment= new GenericClassDisplay<Departement>(new Employee(1, "aaa", "aaa", 0).getClass().getAnnotation(Departement.class));

		displayString.genericDisplay();
		displayinteger.genericDisplay();
		displayboolean.genericDisplay();
		displayEmployyeDepartment.genericDisplay();
		
		
		
		

	}

}
