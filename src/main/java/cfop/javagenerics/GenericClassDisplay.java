package cfop.javagenerics;

public class GenericClassDisplay<T> {
	T data;

	public GenericClassDisplay(T data) {
		super();
		this.data = data;
	}
	
	public void genericDisplay(){
		System.out.println(data.toString());
		
	}
}
