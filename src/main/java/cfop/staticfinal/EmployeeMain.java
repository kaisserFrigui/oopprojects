package cfop.staticfinal;

public class EmployeeMain {

	final long id;
	String name;
	double baseSalary;
	static final String COMPANY_NAME = "google";
	static double extraBonus;

	
	public EmployeeMain(long id, String name, double baseSalary) {
		this.id = id;
		this.name = name;
		this.baseSalary = baseSalary;
	}

	@Override
	public String toString() {
		return "EmployeeMain [id=" + id + ", name=" + name + ", baseSalary=" + baseSalary + "]";
	}

	public static void main(String[] args) {

		EmployeeMain emp = new EmployeeMain(1, "ahmed", 22.5);
		emp.extraBonus=23;
		System.out.println(emp.extraBonus);
		
		EmployeeMain emp2 = new EmployeeMain(2, "ali", 22.5);
		emp2.extraBonus=15;
		System.out.println(emp2.extraBonus);



	}

}
