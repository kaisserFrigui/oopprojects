package cfop.abstraction;

public class MainAbstraction {

	public static void main(String[] args) {
		
		Drawable rectangle= new Rectangle();
		rectangle.draw();
		Drawable circle= new Circle();
		circle.draw();

	}

}
