package cfop.abstraction;

public abstract class Drawable {
	
	abstract void draw();

}
