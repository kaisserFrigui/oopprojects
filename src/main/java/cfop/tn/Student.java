package cfop.tn;

import java.util.Objects;

public class Student {
	
	private long Id;
	private String FirstName;
	private String LastName;
	private int age;
	
	
	public Student() {
		super();
	}
	
	
	public Student(long id, String firstName, String lastName, int age) {
		super();
		Id = id;
		FirstName = firstName;
		LastName = lastName;
		this.age = age;
	}


	public long getId() {
		return Id;
	}
	public String getFirstName() {
		return FirstName;
	}
	public void setFirstName(String firstName) {
		FirstName = firstName;
	}
	public String getLastName() {
		return LastName;
	}
	public void setLastName(String lastName) {
		LastName = lastName;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}


	@Override
	public int hashCode() {
		return Objects.hash(FirstName, Id, LastName, age);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		return Objects.equals(FirstName, other.FirstName) && Id == other.Id && Objects.equals(LastName, other.LastName)
				&& age == other.age;
	}


	@Override
	public String toString() {
		return "Student [Id=" + Id + ", FirstName=" + FirstName + ", LastName=" + LastName + ", age=" + age + "]";
	}
	
	
	

}
