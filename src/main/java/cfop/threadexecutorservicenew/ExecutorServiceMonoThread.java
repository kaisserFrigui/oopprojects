package cfop.threadexecutorservicenew;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import cfop.threads.MyRunnableThread;

public class ExecutorServiceMonoThread {

	public static void main(String[] args) throws InterruptedException, ExecutionException {
		createThreadWithExecute();
		createThreadWithSubmit();
	}

	private static void createThreadWithSubmit() throws InterruptedException, ExecutionException {
		ExecutorService service = null;
		try {
			service = Executors.newSingleThreadExecutor();
			Future<Integer> result=service.submit(()-> 1+1);
			System.out.println(result.get());
		} finally {
			if (service != null)
				service.shutdown();

		}
	}

	private static void createThreadWithExecute() {
			ExecutorService service = null;
			try {
				service = Executors.newSingleThreadExecutor();
				service.execute(new MyRunnableThread());
			} finally {
				if (service != null)
					service.shutdown();
	
			}
	}

}
