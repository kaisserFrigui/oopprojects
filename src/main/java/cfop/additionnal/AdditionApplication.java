package cfop.additionnal;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.*;
import java.util.stream.Stream;

interface Addable{  
    int add(int a,int b);  
}

public class AdditionApplication {

	public static void main(String[] args) {
		// Multiple parameters in lambda expression  
        Addable ad1=(a,b)->(a+b);  
        System.out.println(ad1.add(10,20));  
          
        // Multiple parameters with data type in lambda expression  
        Addable ad2=(int a,int b)->(a+b);  
        System.out.println(ad2.add(100,200));  
        
        
        Consumer<String> printConsumer = t -> System.out.println(t);
        Stream<String> cities = Stream.of("Sydney", "Dhaka", "New York", "London");
        cities.forEach(printConsumer);
        //accepts an arguments and return no results
        Consumer<Integer> consumer = (value) -> System.out.println(value);
        consumer.accept(25);
        //entrée valeur -> boolean 
        Predicate predicate = (value) -> value != null;
        System.out.println(predicate.test(""));;
        
        List<String> names = Arrays.asList("John", "Smith", "Samueal", "Catley", "Sie");
        Predicate<String> nameStartsWithS = str -> str.startsWith("S");
        names.stream().filter(nameStartsWithS).forEach(System.out::println);
        
        //entrée aucun inputs return result
        Supplier<Integer> supplier=()->new Random().nextInt();
        System.out.println(supplier.get());
 

	}

}
