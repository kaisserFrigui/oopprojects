package cfop.threads;

public class MyRunnableThread implements Runnable {

	@Override
	public void run() {
		System.out.println("Thread" + "   " + Thread.currentThread().getName() + "   " + "is executing a task");
		try {
			Thread.sleep(200000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		Thread myth = new Thread(new MyRunnableThread());
		myth.start();

		try {
			Thread.sleep(200000);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
