package cfop.poolthread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolExecutorService {
	
	private int taskExecutedCount = 0;

	private  void incrementAndReport() {
		System.out.println(Thread.currentThread().getName()+ " :" +(++taskExecutedCount)
				+ " task (s)");
	}

	public static void main(String[] args) {
		ExecutorService service = null;
		try {
			service = Executors.newFixedThreadPool(16);
			ThreadPoolExecutorService incrementCountTaskService = new ThreadPoolExecutorService();
			for (int i = 0; i < 10; i++)
				service.submit(() -> incrementCountTaskService.incrementAndReport());
		} finally {
			if (service != null)
				service.shutdown();
		}
	}

}
