package cfop.inheritance;

public class Animal {
	
	private long Id;
	private boolean vegetrian;
	private  String color;
	private int numberOfLegs;

	public Animal(long id, boolean vegetrian, String color, int numberOfLegs) {
		super();
		Id = id;
		this.vegetrian = vegetrian;
		this.color = color;
		this.numberOfLegs = numberOfLegs;
	}
	
	
	public void makeNoise() {
		
		System.out.println("Animal make Noise");
	}
	
	
	public boolean isVegetrian() {
		return vegetrian;
	}
	public void setVegetrian(boolean vegetrian) {
		this.vegetrian = vegetrian;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getNumberOfLegs() {
		return numberOfLegs;
	}
	public void setNumberOfLegs(int numberOfLegs) {
		this.numberOfLegs = numberOfLegs;
	}
	public long getId() {
		return Id;
	}
	@Override
	public String toString() {
		return "Animal [Id=" + Id + ", vegetrian=" + vegetrian + ", color=" + color + ", numberOfLegs=" + numberOfLegs
				+ "]";
	}
	
	

	
	
	

}
