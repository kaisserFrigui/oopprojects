package cfop.inheritance;

public class MainInheritance {

	public static void main(String[] args) {

		Animal cat=new Cat(1,true,"Yellow",4);
		//System.out.println(cat.toString());
		cat.makeNoise();
		
		Animal dog=new Dog(1, false, "Black", 0);
		//System.out.println(dog.toString());
		dog.makeNoise();
	

	}

}
