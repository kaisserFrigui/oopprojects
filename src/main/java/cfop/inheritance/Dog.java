package cfop.inheritance;

public class Dog extends Animal {

	public Dog(long id, boolean vegetrian, String color, int numberOfLegs) {
		super(id, vegetrian, color, numberOfLegs);
		
	}

	@Override
	public String toString() {
		return "Dog [isVegetrian()=" + isVegetrian() + ", getColor()=" + getColor() + ", getNumberOfLegs()="
				+ getNumberOfLegs() + ", getId()=" + getId() + ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + "]";
	}


	@Override
	public void makeNoise() {
		
		System.out.println("Dog makes bark");

	}

	

}
