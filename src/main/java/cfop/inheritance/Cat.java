package cfop.inheritance;

public class Cat extends Animal {

	public Cat(long id, boolean vegetrian, String color, int numberOfLegs) {
		super(id, vegetrian, color, numberOfLegs);
	}

	@Override
	public String toString() {
		return "Cat [isVegetrian()=" + isVegetrian() + ", getColor()=" + getColor() + ", getNumberOfLegs()="
				+ getNumberOfLegs() + ", getId()=" + getId() + ", toString()=" + super.toString() + ", getClass()="
				+ getClass() + "]";
	}

	@Override
	public void makeNoise() {
		
	System.out.println("Cat makes mewww ");
	}
	
	

}
