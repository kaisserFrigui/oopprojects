package cfop.optional;

import java.util.Optional;

import cfop.annotation.Manager;




public class OptionalApplication {
	
	public static void main(String[] args) {
		
		Optional<Manager> manager = Optional.ofNullable(new Manager(1, "Kaisser", "CEO", 0) );
		if (manager.isPresent())
			System.out.println("state est " + manager.get());
		else 
			System.out.println("No state available : ");

	}
	
}
