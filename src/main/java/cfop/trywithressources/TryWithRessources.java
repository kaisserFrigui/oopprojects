package cfop.trywithressources;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class TryWithRessources {

	public static void main(String[] args) {

		readFileWithoutTryRessources();
		readFileWithTryRessources();

	}

	private static void readFileWithoutTryRessources() {
		BufferedReader br = null;
		try {
			String sCurrentLine;
			br = new BufferedReader(new FileReader("content.txt"));
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
			}
		} catch (IOException e) {
			// handle exception
			e.printStackTrace();
		} finally {
			try {
				if (br != null)
					br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	private static void readFileWithTryRessources() {
		try (BufferedReader br = new BufferedReader(new FileReader("content.txt"))) {
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				System.out.println(sCurrentLine);
			}
		} catch (IOException e) {
			// handle exception
			e.printStackTrace();
		}
	}

}
