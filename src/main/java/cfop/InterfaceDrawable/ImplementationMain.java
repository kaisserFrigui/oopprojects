package cfop.InterfaceDrawable;

import cfop.abstraction.Circle;
import cfop.abstraction.Drawable;
import cfop.abstraction.Rectangle;

public class ImplementationMain {

	public static void main(String[] args) {
		
		IDrawable rect=new RectangleImp();
		rect.draw();
		IDrawable circl=new CircleImpl();
		circl.draw();
		
		
	
	}

}
