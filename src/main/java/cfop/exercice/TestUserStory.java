package cfop.exercice;

public class TestUserStory {

	public static void main(String[] args) {

		Employee programmer = new Programmer(1, "kaisser", "Programmer", 1200, 200, 50);
		System.out.println(programmer.toString());
		System.out.println(programmer.specialBonusOrExtraTime());

		Employee manager = new Manager(2,"Jihed","Manager",1300,300,40);
		System.out.println(manager.toString());
		System.out.println(manager.specialBonusOrExtraTime());
	}

}
