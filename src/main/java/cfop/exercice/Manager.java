package cfop.exercice;

public class Manager extends Employee {
	
	float extraBonus;
	public Manager(long id, String name, String role, float baseSalary, float bonus,float extraBonus) {
		super(id, name, role, baseSalary, bonus);
		this.extraBonus=extraBonus;
	}

	@Override
	public double specialBonusOrExtraTime() {
		System.out.println("Im a Manager and i have a special Bonus");
		return super.specialBonusOrExtraTime()+extraBonus; 
	}

	@Override
	public String toString() {
		return "Manager [extraBonus=" + extraBonus + ", getName()=" + getName() + ", getRole()=" + getRole()
				+ ", getBaseSalary()=" + getBaseSalary() + ", getBonus()=" + getBonus() + ", getClass()=" + getClass()
				+ "]";
	}



}
