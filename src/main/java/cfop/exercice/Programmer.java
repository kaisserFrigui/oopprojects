package cfop.exercice;

import java.sql.Time;

public class Programmer extends Employee {
		double extraTimePayment;
	public Programmer(long id, String name, String role, float baseSalary, float bonus,double extraTimePayment) {
		super(id, name, role, baseSalary, bonus);
		this.extraTimePayment=extraTimePayment;
		
	}

	@Override
	public double specialBonusOrExtraTime() {
	System.out.println("Im a programmer and i have an extraTimePayment");
	return super.specialBonusOrExtraTime()+extraTimePayment;
	}

	@Override
	public String toString() {
		return "Programmer [extraTimePayment=" + extraTimePayment + ", getName()=" + getName() + ", getRole()="
				+ getRole() + ", getBaseSalary()=" + getBaseSalary() + ", getBonus()=" + getBonus() + ", getClass()="
				+ getClass() + "]";
	}


	

}
