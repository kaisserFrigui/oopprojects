package cfop.exercice;

public class Employee {

	private long Id;
	private String name;
	private String role;
	private float baseSalary;
	private float bonus;

	public Employee(long id, String name, String role, float baseSalary, float bonus) {
		super();
		Id = id;
		this.name = name;
		this.role = role;
		this.baseSalary = baseSalary;
		this.bonus = bonus;
	}

	public double specialBonusOrExtraTime() {
		return baseSalary+bonus;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public float getBaseSalary() {
		return baseSalary;
	}

	public void setBaseSalary(float baseSalary) {
		this.baseSalary = baseSalary;
	}

	public float getBonus() {
		return bonus;
	}

	public void setBonus(float bonus) {
		this.bonus = bonus;
	}

	public long getId() {
		return Id;
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", role=" + role + ", baseSalary=" + baseSalary + ", bonus=" + bonus + "]";
	}

}
