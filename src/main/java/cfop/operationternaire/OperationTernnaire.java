package cfop.operationternaire;

public class OperationTernnaire {

	public static void main(String[] args) {

		int number = 8;
		verifyNumberWithSimpleTest(number);
		System.out.println("-------------------------------------");
		String message=verifyNumberWithOperationTernaireTest(number);
		System.out.println("result with ternary:"+message);
	}

	private static String verifyNumberWithOperationTernaireTest(int number) {
		return (number > 8) ? "Number greater than 8" : "Number less or equals than 8";
	}

	private static void verifyNumberWithSimpleTest(int number) {
		if (number > 8)
			System.out.println("Number greater than 8");
		else
			System.out.println("Number less or equals than 8");
	}

}
