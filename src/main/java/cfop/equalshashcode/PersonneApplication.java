package cfop.equalshashcode;

import java.util.Objects;

public class PersonneApplication {

	public static void main(String[] args) {
		
		Personne personne1= new Personne(1, "Kaisser", "Frigui", 30);
		Personne personne2= new Personne(1, "Kaisser", "Frigui", 30);
		System.out.println(personne1.equals(personne2));
		
		int p1=personne1.hashCode();
		int p2=personne2.hashCode();
		System.out.println(p1);
		System.out.println(p2);

		System.out.println(p1==p2);
		
	

	}

}

class Personne {
	private long id;
	private String firstName;
	private String lastName;
	private int age;

	public Personne(long id, String firstName, String lastName, int age) {
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	public long getId() {
		return id;
	}


	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, firstName, id, lastName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Personne other = (Personne) obj;
		return age == other.age && Objects.equals(firstName, other.firstName) && id == other.id
				&& Objects.equals(lastName, other.lastName);
	}

	
	

}
