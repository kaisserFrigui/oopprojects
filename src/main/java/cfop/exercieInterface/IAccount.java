package cfop.exercieInterface;

import cfop.specException.UnuthorizedWithdrawException;

public interface IAccount {
	
	void withdraw(double amount) throws UnuthorizedWithdrawException ;
	void  deposit(double amount);
	

}
