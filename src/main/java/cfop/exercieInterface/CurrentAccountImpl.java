package cfop.exercieInterface;

public class CurrentAccountImpl implements IAccount {

	double balanceSavingAccount;

	@Override
	public void withdraw(double amount) {
		if (balanceSavingAccount - amount <= 0) {
			System.err.println(" you can't withdraw your money");
		}
	}

	@Override
	public void deposit(double amount) {
		balanceSavingAccount = balanceSavingAccount + amount;
		System.out.println(balanceSavingAccount);
	}

}
