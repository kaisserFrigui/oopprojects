package cfop.exercieInterface;

import cfop.specException.UnuthorizedWithdrawException;

public class TestImplExercice {

	public static void main(String[] args) {

		IAccount sAccount = new SavingAccountImpl();
		IAccount cAccount = new CurrentAccountImpl();
//		sAccount.deposit(10);
		try {
			sAccount.withdraw(10);
		} catch (UnuthorizedWithdrawException e) {
			System.out.println("Code app:" + e.getCode() + "   " + "Message: " + e.getMessage());
		}
//		cAccount.deposit(100);
		try {
			cAccount.withdraw(0);
		} catch (UnuthorizedWithdrawException e) {

			System.out.println("Code app:" + e.getCode() + "   " + "Message: " + e.getMessage());

		}

	}

}
