package cfop.exercieInterface;

import cfop.specException.UnuthorizedWithdrawException;

public class SavingAccountImpl implements IAccount {

	private static final String MESSAGE_ERROR_02 = "You can't withdraw your money because you have riched a maximum fixed amount 100";

	private static final String MESSAGE_ERROR_01 = "You can't withdraw your money";

	private static final String CODE_ERROR_O2 = "WITHDRAW-02";

	private static final String CODE_ERROR_01 = "WITHDRAW-01";

	private static final int LIMIT_AMOUNT = 100;

	double balanceSavingAccount;

	@Override
	public void withdraw(double amount) throws UnuthorizedWithdrawException {

		if (balanceSavingAccount - amount <= 0) {

			throw new UnuthorizedWithdrawException(CODE_ERROR_01, MESSAGE_ERROR_01);

		}
		if (balanceSavingAccount <= LIMIT_AMOUNT) {
			throw new UnuthorizedWithdrawException(CODE_ERROR_O2, MESSAGE_ERROR_02);
		}

	}

	@Override
	public void deposit(double amount) {

		balanceSavingAccount = balanceSavingAccount + amount;
		System.out.println(balanceSavingAccount);
	}

}
